BUILD ?= Debug
#BUILD = Release

CC = gcc
CXX = g++
MPIC = mpicc

CCFLAGS = -g
CXXFLAGS = -g -std=c++11


ifeq ($(BUILD), Release)
CXXFLAGS += -O3 -ffast-math -ftree-vectorize -funroll-loops
CCFLAGS += -O3 -ffast-math -ftree-vectorize -funroll-loops
endif

ifeq ($(BUILD), Debug)
CXXFLAGS += -Wall -O0
CCFLAGS += -Wall -O0 -DDEBUG
endif

all: clean pg p0 p1 p2 p3 p4 p5

pg:
	$(CXX) $(CXXFLAGS) gen.cpp -o pg

p0:
	$(CC) $(CCFLAGS) dgemv_0.c -o p0

p1:
	$(MPIC) $(CCFLAGS) dgemv_1.c -o p1

p2:
	$(MPIC) $(CCFLAGS) dgemv_2.c -o p2

p3:
	$(MPIC) $(CCFLAGS) dgemv_3.c -o p3

p4:
	$(MPIC) $(CCFLAGS) dgemv_4.c -o p4

p5:
	$(MPIC) $(CCFLAGS) dgemv_5.c -o p5

clean:
	rm -rf p*
