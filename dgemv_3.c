#include "matrix.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <mpi.h>

int main (int argc, char** argv) {

    MPI_Init(&argc, &argv);

    if (argc != 2) {
        printf("Please, enter size of matrix!\n");
        MPI_Finalize();
        return 0;
    }

    int world_size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

//READ SRC VECTOR && MATRIX
    uint32_t vrows = 0, vcols = 0;
    uint32_t nrows = 0, ncols = 0;
    double *local_val = NULL, *vals = NULL;
    double *local_src = NULL, *src = NULL;
    double *local_block = NULL;

    if (rank == 0) {
        src = read_matrix(&vrows, &vcols, VECTOR, argv[1]);
        vals = read_matrix(&nrows, &ncols, MATRIX, argv[1]);
    }

    MPI_Bcast(&nrows, 1, MPI_UINT32_T, 0, MPI_COMM_WORLD);
    MPI_Bcast(&ncols, 1, MPI_UINT32_T, 0, MPI_COMM_WORLD);

    if (ncols == 0 || nrows == 0) {
        MPI_Finalize();
        return 2;
    }

    double* dest = (double*)calloc(nrows, sizeof(double));

    int block_rows = nrows / world_size, extra_rows = nrows % world_size;
    int block_cols = ncols / world_size, extra_cols = ncols % world_size;
    int rows = block_rows + extra_rows, cols = block_cols;
    if (rank == 0) {
        cols += extra_cols;
    }
    //Send SRC to Procs!
    int sendcounts[world_size];
    int disps[world_size];
    int l;
    for (l = 0; l < world_size; l++) {
        sendcounts[l] = block_cols;
        disps[l] = block_cols * l + extra_cols;
    }
    sendcounts[0] += extra_rows;
    disps[0] = 0;
    local_src = (double*)malloc(sendcounts[rank] * sizeof(double));
    MPI_Scatterv(src, sendcounts, disps, MPI_DOUBLE, local_src, sendcounts[rank], MPI_DOUBLE, 0, MPI_COMM_WORLD);

    int niters = 3, k;
    for (k = 0; k < niters; k++) {
        MPI_Barrier(MPI_COMM_WORLD);
        printf("Iter: %d\n", k);
        double t1 = MPI_Wtime();

        int band_num, i, j;
        for (band_num = 0; band_num < world_size; band_num++) {
        //1. cut current band into blocks.
        // * first process have extra_cols.
            int row_disp = band_num * rows;
            if (band_num != 0) row_disp += extra_rows;
            if (rank == 0) {
                local_block = (double*)malloc(rows * block_cols * sizeof(double));
                int block_num, pos;
            //First loop: iters on blocks
                for (block_num = 1; block_num < world_size; block_num++) {
                    pos = 0;
            //Second loop: iters on rows inside block
                    for (i = row_disp; i < row_disp + rows; i++) {
            //Third loop: iters on cols inside row in block
                        for (j = extra_cols + block_num * block_cols; j < extra_cols + (block_num+1) * block_cols; j++)
                            local_block[pos++] = vals[i * ncols + j];
                    }
                    MPI_Send(local_block, rows * block_cols, MPI_DOUBLE, block_num, 0, MPI_COMM_WORLD);
                }
            //Create contignous data for first process (with extra-cols)
                pos = 0;
                local_block = (double*)realloc(local_block, rows * cols * sizeof(double));
                for (i = row_disp; i < row_disp + rows; i++)
                    for (j = 0; j < cols; j++)
                        local_block[pos++] = vals[i * ncols + j];

                local_val = local_block;
            } else {
                local_val = (double*)malloc(rows * cols * sizeof(double));
                MPI_Recv(local_val, rows * cols, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }

        //2. Allocate destination vector and then compute.
            double* work_dest = (double*)calloc(rows, sizeof(double));

            for (i = 0; i < rows; i++)
                work_dest[i] = 0;

            for (i = 0; i < rows; i++) {
                for (j = 0; j < cols; j++) {
                    work_dest[i] += local_val[i*cols + j] * local_src[j];
                }
            }

        //3. Collect data from work_destination to Destination.
            MPI_Allreduce(work_dest, dest+row_disp, rows, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

        //4. Do something else..
            rows = block_rows;
            free(local_val);
            free(work_dest);
        }

        MPI_Barrier(MPI_COMM_WORLD);
        double t2 = MPI_Wtime();
        printf("From rank [%d]: time: %f\n", rank, t2-t1);
    }

#ifdef DEBUG
    for (k = 0; k < vrows; k++)
        printf("%f ", dest[k]);
    printf("\n");
#endif

    if (rank == 0) free(vals);
    free(src);
    free(local_src);
    free(dest);
    MPI_Finalize();
    return 0;
}
