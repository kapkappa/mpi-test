#include "matrix.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <mpi.h>

int main (int argc, char** argv) {

    MPI_Init(&argc, &argv);

    if (argc != 2) {
        printf("Please, enter size of matrix!\n");
        MPI_Finalize();
        return 0;
    }

    int world_size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

//READ SRC VECTOR
//READ MATRIX and SRC VECTOR
    double *local_val = NULL, *vals = NULL;
    double *local_src = NULL, *src = NULL;

    uint32_t nrows = 0, ncols = 0;
    uint32_t vrows, vcols;
    if (rank == 0) {
        vals = read_matrix(&nrows, &ncols, MATRIX, argv[1]);
        src = read_matrix(&vrows, &vcols, VECTOR, argv[1]);
    }

    MPI_Bcast(&nrows, 1, MPI_UINT32_T, 0, MPI_COMM_WORLD);
    MPI_Bcast(&ncols, 1, MPI_UINT32_T, 0, MPI_COMM_WORLD);

    if (ncols == 0 || nrows == 0) {
        MPI_Finalize();
        return 2;
    }

    //I need chunks!
    int block_rows = nrows / world_size, extra_rows = nrows % world_size;
    int block_cols = ncols / world_size, extra_cols = ncols % world_size;
    int rows = block_rows, cols = block_cols;
    if (rank == 0) {
        rows += extra_rows;
        cols += extra_cols;
        local_val = vals;
    } else {
        local_val = (double*)malloc(sizeof(double) * rows * ncols);
    }

    local_src = (double*)malloc((block_cols + extra_cols) * sizeof(double));

    int sendcounts[world_size];
    int recvcounts[world_size];
    int disps[world_size];
    int l;

    //Send Matrices to procs
    for (l = 0; l < world_size; l++) {
        sendcounts[l] = block_rows * ncols;
        disps[l] = block_rows * ncols * l + extra_rows * ncols;
    }
    sendcounts[0] += extra_rows * ncols;
    disps[0] = 0;

    MPI_Scatterv(vals, sendcounts, disps, MPI_DOUBLE, local_val, sendcounts[rank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
    //Send SRC to procs
    for (l = 0; l < world_size; l++) {
        sendcounts[l] = block_cols;
        disps[l] = block_cols * l + extra_cols;
    }
    sendcounts[0] += extra_cols;
    disps[0] = 0;

    MPI_Scatterv(src, sendcounts, disps, MPI_DOUBLE, local_src, sendcounts[rank], MPI_DOUBLE, 0, MPI_COMM_WORLD);

//DEST Vector
    double* dest = (double*)calloc(nrows, sizeof(double));
    double* work_dest = (double*)malloc(rows * sizeof(double));
    double *tmp = NULL;
    double *recv_local_src = (double*)malloc((block_cols + extra_cols) * sizeof(double));

    MPI_Request rec_reqv, sen_reqv;

    int i, j, k, niters = 3;
    for (k = 0; k < niters; k++) {
        MPI_Barrier(MPI_COMM_WORLD);
        printf("Iter: %d\n", k);
        double t1 = MPI_Wtime();

        for (l = 0; l < rows; l++) work_dest[l] = 0;

        for (l = 0; l < world_size; l++) {
            int next_pos = (rank+1+l) % world_size;
            int pos = (rank + l) % world_size;
            int rec_tag = pos, sen_tag = rank;

            MPI_Irecv(recv_local_src, sendcounts[next_pos], MPI_DOUBLE, next_pos, rec_tag, MPI_COMM_WORLD, &rec_reqv);
            MPI_Isend(local_src, cols, MPI_DOUBLE, (rank + world_size - l - 1) % world_size, sen_tag, MPI_COMM_WORLD, &sen_reqv);

    //            MPI_Send(local_src, cols, MPI_DOUBLE, (rank + world_size -l -1) % world_size, sen_tag, MPI_COMM_WORLD);
    //            MPI_Recv(recv_local_src, sendcounts[pos], MPI_DOUBLE, pos, rec_tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            for (i = 0; i < rows; i++) {
                for (j = 0; j < sendcounts[pos]; j++) {
                    work_dest[i] += local_val[i * ncols + disps[pos] + j] * local_src[j];
                }
            }

//             MPI_Wait(&sen_reqv, MPI_STATUS_IGNORE);
            MPI_Wait(&rec_reqv, MPI_STATUS_IGNORE);

            tmp = local_src;
            local_src = recv_local_src;
            recv_local_src = tmp;

        }

        for (i = 0; i < rows; i++) {
            for (j = 0; j < cols; j++) {
                work_dest[i] += local_val[i * ncols + disps[rank] + j] * local_src[j];
            }
        }


        for (i = 0; i < world_size; i++) {
            recvcounts[i] = block_rows;
            disps[i] = extra_rows + block_rows * i;
        }
        disps[0] = 0;
        recvcounts[0] += extra_rows;

        MPI_Allgatherv(work_dest, rows, MPI_DOUBLE, dest, recvcounts, disps, MPI_DOUBLE, MPI_COMM_WORLD);

        MPI_Barrier(MPI_COMM_WORLD);
        double t2 = MPI_Wtime();

        printf("From rank [%d]: time: %f\n", rank, t2-t1);
    }

#ifdef DEBUG
    for (i = 0; i < ncols; i++)
        printf("%f ", dest[i]);
    printf("\n");
#endif

    if (rank == 0) free(vals);
    else free(local_val);
    free(src);
    free(local_src);
    free(recv_local_src);
    free(dest);
    free(work_dest);
    MPI_Finalize();
    return 0;
}
