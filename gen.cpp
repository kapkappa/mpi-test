#include <string.h>
#include <iomanip>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <random>
#include <sys/time.h>

using namespace std;

double timer() {
    struct timeval tp;
    struct timezone tzp;
    gettimeofday(&tp, &tzp);
    return ((double)tp.tv_sec+(double)tp.tv_usec * 1.e-6);
}

struct dense {
    uint32_t nrows;
    uint32_t ncols;
    uint32_t elems;

    std::vector<double> val;

    double*vals;

    bool if_initialized;

    dense() {
        nrows = ncols = 0;
        elems = 0;
        if_initialized = false;
        vals = nullptr;
    }

    dense(uint32_t nrow, uint32_t ncol) {
        if_initialized = false;
        nrows = nrow;
        ncols = ncol;
        elems = nrows * ncols;
        vals = (double*)malloc(elems*sizeof(double));
        for(uint32_t i = 0; i < elems; i++)
            val.push_back(0);
    }

    dense(const dense &T) {
        nrows = T.nrows;
        ncols = T.ncols;
        elems = T.elems;
        if_initialized = T.if_initialized;
        val.resize(elems);
        val = T.val;
//FIXME!
        vals = nullptr;
    }

    void fill() {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> dis_left_boarder(-15.0, 0.0);
        std::uniform_real_distribution<> dis_right_boarder(0.0, 15.0);
        std::extreme_value_distribution<double> dis(dis_left_boarder(gen), dis_right_boarder(gen));

        cout << dis.a() << " " << dis.b() << endl;

        for (uint32_t i = 0; i < nrows; i++) {
            for (uint32_t j = 0; j < ncols; j++) {
                vals[i * ncols + j] = dis(gen);
/*
                double value = dis(gen);
                cout << value << ' ';
                val[i*nrows+j] = value;
                cout << val[i*nrows+j] << endl;
*/
            }
        }
        if_initialized = true;
    }

    void print() {
        cout << "Matrix, size: " << nrows << " " << ncols << endl;
        for (uint32_t i = 0; i < nrows; i++) {
            for (uint32_t j = 0; j < ncols; j++)
                cout << setw(10) << vals[i * ncols + j] << " ";
            cout << endl;
        }
    }

    void write(string fname) {
        FILE*f=fopen(fname.c_str(), "w");
        fwrite(&nrows, sizeof(uint32_t), 1, f);
        fwrite(&ncols, sizeof(uint32_t), 1, f);
        for (uint32_t i = 0; i < nrows; i++) {
            for (uint32_t j = 0; j < ncols; j++) {
                fwrite(&vals[i*ncols+j], sizeof(double), 1, f);
            }
        }
        fclose(f);
    }

    ~dense() {free(vals);}

};

dense multiply(const dense & A, const dense & B) {
    dense C(A.nrows, B.ncols);
    if (A.ncols != B.nrows) {
        cout << "Mismatched dimensions" << endl;
        return C;
    }

    for (uint32_t i = 0; i < A.nrows; i++)
        for (uint32_t j = 0; j < B.ncols; j++)
            for (uint32_t k = 0; k < A.ncols; k++)
                C.val[i * A.nrows + j] += A.val[i * A.nrows + k] * B.val[k * B.nrows + j];

    return C;
}

dense multiply_vinogradov(const dense & A, const dense & B) {
    dense C(A.nrows, B.ncols);
    if (A.ncols != B.nrows) {
        cout << "Mismatched dimensions" << endl;
        return C;
    } else if (A.ncols % 2 != 0) {
        cout << "Inappropriate dimension" << endl;
        return C;
    }
    uint32_t m = A.ncols / 2;
    for (uint32_t i = 0; i < A.nrows; i++) {
        for (uint32_t j = 0; j < B.ncols; j++) {
            for (uint32_t k = 0; k < m; k++)
                C.val[i*A.nrows+j] += (A.val[i*A.nrows + 2*k] + B.val[(2*k+1)*B.nrows + j]) * (B.val[(2*k)*B.nrows+j] + A.val[i*A.nrows+2*k+1]);
            for (uint32_t k = 0; k < m; k++) {
                C.val[i*A.nrows+j] -= A.val[i*A.nrows+2*k+1]*A.val[i*A.nrows+2*k];
                C.val[i*A.nrows+j] -= B.val[(2*k+1)*B.nrows+j]*B.val[2*k*B.nrows+j];
            }
        }
    }
    return C;
}

int main(int argc, char** argv) {
    uint32_t row1, col1, row2, col2;
    cout << "Enter first matrix domensions:\n";
    cin >> row1;
    cin >> col1;
    dense A(row1, col1);
    A.fill();
//    A.print();

    cout << "Enter matrix filename\n";
    string filename;
    cin >> filename;

    A.write(filename);
    return 0;

    cout << "Enter second matrix domensions:\n";
    cin >> row2;
    cin >> col2;
    dense B(row2, col2);
    B.fill();

double t1 = timer();
    dense C = multiply(A, B);
double t2 = timer();
    cout << "time required: " << t2-t1 << endl;
//    C.print();

double t3 = timer();
    dense D = multiply_vinogradov(A, B);
double t4 = timer();
    cout << "time required: " << t4-t3 << endl;
//    D.print();

    return 0;
}
