#include "matrix.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <mpi.h>

int main (int argc, char** argv) {

    MPI_Init(&argc, &argv);

    if (argc != 2) {
        printf("Please, enter size of matrix!\n");
        MPI_Finalize();
        return 0;
    }

    int world_size; //number of procs
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    int rank;       //rank of cur proc
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

//READ MATRIX
    uint32_t nrows, ncols;
    double *vals = read_matrix(&nrows, &ncols, MATRIX, argv[1]);
    if (vals == 0) {
        MPI_Finalize();
        return 1;
    }
//READ SRC VECTOR
    uint32_t vrows, vcols;
    double *src = read_matrix(&vrows, &vcols, VECTOR, argv[1]);
    if (src == 0) {
        MPI_Finalize();
        return 2;
    }
//DEST Vector
    int block_rows = nrows / world_size;
    int extra_rows = nrows % world_size;
    int rows = block_rows;
    if (rank == world_size-1) rows += extra_rows;    //last proc handle extra rows

    double* dest = (double*)calloc(nrows, sizeof(double));
    double* work_dest = (double*)malloc(rows * sizeof(double));

    int i, j, k, niters = 3;
    for (k = 0; k < niters; k++) {
        MPI_Barrier(MPI_COMM_WORLD);
        printf("Iter: %d\n", k);
        double t1 = MPI_Wtime();

        for (i = 0; i < rows; i++) work_dest[i] = 0;
        for (i = 0; i < rows; i++) {
            for (j = 0; j < ncols; j++) {
                work_dest[i] += vals[(rank*block_rows+i)*ncols + j] * src[j];
            }
        }

        int recvcount[world_size];
        int disps[world_size];
        for (i = 0; i < world_size; i++) {
            recvcount[i] = block_rows;
            disps[i] = block_rows * i;
        }
        recvcount[world_size-1] += extra_rows;

        MPI_Allgatherv(work_dest, rows, MPI_DOUBLE, dest, recvcount, disps, MPI_DOUBLE, MPI_COMM_WORLD);

        MPI_Barrier(MPI_COMM_WORLD);
        double t2 = MPI_Wtime();
        printf("From rank [%d]: time: %f\n", rank, t2-t1);
    }

#ifdef DEBUG
    for (i = 0; i < vrows; i++)
        printf("%f ", dest[i]);
    printf("\n");
#endif
    free(vals);
    free(src);
    free(dest);
    free(work_dest);
    MPI_Finalize();
    return 0;
}
